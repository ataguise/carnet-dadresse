@echo off
title Garnet d'adresse
color 1f

CLS

echo Compilation...
javac @options @classes

IF ERRORLEVEL 1 goto FIN

echo Aucune erreur, good job !

echo Execution...
pause
CLS
java -cp bin Application

goto FIN2

:FIN
pause
goto FIN2

:FIN2
@echo on