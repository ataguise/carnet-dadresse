package modele;

import java.util.*;
import java.util.regex.*;
import java.io.Serializable;

/**
  * Classe Carnet : définit ce qu'est un carnet d'adresse en théorie
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Carnet implements Serializable{
	private static final long serialVersionUID = 1350092881346723535L;
	/**
	  * Liste de contacts du carnet
	  */
	private ArrayList<Personne> contacts, sousContacts;

	private HashMap<String,Integer> tabasco;

	private Boolean pro;

	private Pattern p_numeros;
	private Pattern p_adresses;
	private Pattern p_emails;
	
	/**
	  * Constructeur du carnet, initalise la liste des contacts
	  */
	public Carnet(){
		this.contacts = new ArrayList<Personne>();
		this.tabasco = new HashMap<String, Integer>();

		this.p_numeros = Pattern.compile("((01|02|03|04|05|06|07|09)[0-9]{8})?");
		this.p_adresses = Pattern.compile("([0-9]{1,3} (rue|route|avenue|boulevard) [a-zA-Z /-_]{1,} [0-9]{5} [a-zA-Z /-_]{1,})?");
		this.p_emails = Pattern.compile("([a-zA-Z]{1,}[.-_][a-zA-Z]{1,}@(laposte|hotmail|gmail|yahoo|webmail).(net|fr|com))?");
	}

	/**
	  * Ajoute un contact
	  *
	  * @param personne la personne à ajouter
	  *
	  * @throws DoublonException quand on veut ajouter une personne qui existe déjà
	  */
	public void ajouterContact(Personne personne) throws DoublonException{
		if(this.contacts.contains(personne)){
			throw new DoublonException("Exception : La personne existe déjà.");
		} else {
			this.contacts.add(personne);
			Collections.sort(contacts);

			this.tabasco.put(personne.toString(), personne.getID());
		}
	}

	/**
	 * Renvois l'ID du contact suivant
	 * @param  idcondition ID du contact actuel
	 * @return             ID du contact suivant
	 */
	public int donneContactSuivant(int idcondition){
		int r = -1;
		for(Iterator<Personne> it = contacts.iterator(); it.hasNext(); ){
			Personne p = it.next();

			r = p.getID();

			if(r != idcondition)
				break;
			else if(contacts.size() == 0){
				r = -1;
				break;
			}
		}

		return r;
	}

	/**
	 * Recupère le pattern des numéros
	 * @return Le pattern
	 */
	public Pattern getPatternNumeros(){
		return this.p_numeros;
	}

	/**
	 * Récupère le pattern des adresses
	 * @return Le pattern
	 */
	public Pattern getPatternAdresses(){
		return this.p_adresses;
	}

	/**
	 * Récupère le pattern des emails
	 * @return Le pattern
	 */
	public Pattern getPatternEmails(){
		return this.p_emails;
	}

	/**
	  * Supprime un contact
	  *
	  * @param personne la personne à supprimer
	  */
	public void supprimerContact(int id){
		Personne pers_it = new Personne("","",'d',false);
		for (Iterator<Personne> pers = contacts.iterator(); pers.hasNext(); ){
			pers_it = pers.next();
			if(pers_it.getID() == id){
				break;
			}
		}

		this.contacts.remove(pers_it);
	}

	/**
	  * Modifie un contact en le remplaçant
	  *
	  * @param id l'id de la personne à modifier (et donc remplacer)
	  * @param la nouvelle personne à ajouter en tant que l'ancienne
	  */
	public void modifierContact(int id, Personne personne) throws DoublonException{
		Personne pers_it = new Personne("","",'d',false, id);
		for (Iterator<Personne> pers = contacts.iterator(); pers.hasNext(); ){
			pers_it = pers.next();
			if(pers_it.getID() == id){
				break;
			}
		}

		personne.setID(pers_it.getID());
		this.supprimerContact(pers_it.getID());
		this.ajouterContact(personne);
	}

	/**
	  * Retourne la liste des contacts du carnet
	  *
	  * @return la liste des contacts du carnet
	  */
	public ArrayList<Personne> getContacts(){
		return this.contacts;
	}

	/**
	  * Applique un filtre sur la liste des contacts pour retourner une sous-liste dont le filtre est la première lettre d'un contacts et s'il est proffessionel
	  *
	  * @param lettre le filtre à appliquer
	  * @param profes type de contact à filtrer
	  *
	  * @return une sous-liste filtrée
	  */
	public ArrayList<Personne> sousListe(char lettre, Boolean profes){
		this.sousContacts = new ArrayList<Personne>();

		for (Iterator<Personne> pers = contacts.iterator(); pers.hasNext(); ){
			Personne perso_it = pers.next();
			char[] nom_p = perso_it.getNom().toCharArray();
			
			if(nom_p[0] == lettre){
				pro = perso_it.getPro();
				
				if(pro == profes){
					this.sousContacts.add(perso_it);
				}
			}
		}
		
		return this.sousContacts;
	}

	/**
	 * Renvoie un liens entre le Nom-Prenom d'un contact et son ID
	 * @return La liste liée
	 */
	public HashMap<String,Integer> getTabasco(){
		return this.tabasco;
	}

	/**
	  * Retourne une personne de la liste en fonction de son identifiant
	  *
	  * @param id l'identifiant de la personne à renvoyer
	  *
	  * @return une personne suivant son identifiant
	  */
	public Personne getPersonne(int id){
		Personne pers_it = new Personne("","",'0', false);

		for (Iterator<Personne> pers = contacts.iterator(); pers.hasNext(); ){
			pers_it = pers.next();
			if(pers_it.getID() == id){
				break;
			}
		}

		return pers_it;
	}

	/**
	 * Récupère le nombre de contacts du carnet
	 * @return Le nombre de contact
	 */
	public int getNbPersonnes(){
		return contacts.size();
	}

	/**
	  * Sert à remplir le carnet d'adresse avec un jeu de valeur prédéfini
	  */
	public void remplir(){
		try{
			Personne p1 = new Personne("DEFOIS", "Paul", 'M', false);
			p1.ajouterNumero("Domicile", "0212989324");
			p1.ajouterNumero("Entreprise", "0956565656");
			p1.ajouterNumero("Portable", "0612489524");
			
			ajouterContact(p1);
			ajouterContact(new Personne("DUPONT", "Monsieur", 'M', true));
			ajouterContact(new Personne("DUPOND", "Madame", 'F', false));

		} catch(DoublonException e) {
			System.out.println(e.getMessage());
		}
	}
}