package modele;

import java.io.*;

/**
  * Classe SauvegardeCarnet : permet la sauvegarde et la restauration du modele
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class SauvegardeCarnet{
	private Carnet modele;

	public SauvegardeCarnet(Carnet modele){
		this.modele = modele;
	}

	/**
	  * Permet la sauvegarde du modele
	  */
	public void sauvegarde(){
		try{
			File fichier = new File("tmp/carnet.ser") ;
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichier));
			oos.writeObject(modele);
			oos.close();
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

	/**
	  * Permet la restauration du modele
	  *
	  * @return le modele restauré
	  */
	public Carnet restauration(){
		try{
			File fichier =  new File("tmp/carnet.ser");
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier));

			Carnet m = (Carnet)ois.readObject();

			ois.close();
			return m;
		} catch(IOException e1){
			System.out.println(e1.getMessage());

			return new Carnet();
		} catch(ClassNotFoundException e2){
			System.out.println(e2.getMessage());

			return new Carnet();
		}
	}
}