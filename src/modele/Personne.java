package modele;

import java.util.*;
import java.io.*;


/**
  * Classe Personne : défini ce qu'est une perosnne en théorie
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Personne implements Comparable<Personne>, Serializable{
	private static final long serialVersionUID = 131134672456535L;

	private String nom;
	private String prenom;
	private String nom_societe;
	private char sexe;
	private boolean pro;

	private LinkedHashMap<String, String> numeros;
	private LinkedHashMap<String, String> adresses;
	private LinkedHashMap<String, String> emails;

	private int ID;
	private static int NOMBRE_PERSONNES = 0;

	/**
	  * Constructeur de Personne
	  * Initialise les attributs d'une personne (nom, prenom, ...)
	  * Ainsi que l'ID pour avoir un identifiant unique pour chaque personne
	  *
	  * @param nom le nom de la personne
	  * @param prenom le prenom de la personne
	  * @param sexe le sexe de la personne
	  * @param pro si le contact est professionel ou pas
	  */
	public Personne(String nom, String prenom, char sexe, boolean pro){
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.pro = pro;

		this.numeros = new LinkedHashMap<String, String>();
		this.adresses = new LinkedHashMap<String, String>();
		this.emails = new LinkedHashMap<String, String>();

		NOMBRE_PERSONNES++;
		ID = NOMBRE_PERSONNES;
	}

	/**
	  * Retourne le nombre de personne ajouté depuis le début
	  *
	  * @return le nombre de personne
	  */
	public static int getNombrePersonne(){
		return NOMBRE_PERSONNES;
	}

	/**
	  * Constructeur de Personne, surchagé sauf qu'on impose l'identifiant de contact
	  *
	  * @param nom le nom de la personne
	  * @param prenom le prenom de la personne
	  * @param pro si le contact est professionel ou pas
	  * @param sexe le sexe de la personne
	  * @param id l'identifiant de la personne
	  */
	public Personne(String nom, String prenom, char sexe, boolean pro, int id){
		this(nom, prenom, sexe, pro);

		this.ID = id;
	}

	/**
	  * Défini l'id de la personne
	  *
	  * @param id l'identifiant
	  */
	public void setID(int id){
		this.ID = id;
	}

	/**
	  * Renvoi si deux personnes sont les même ou pas
	  *
	  * @return si deux personnes sont égales
	  */
	public boolean equals(Object o){
		if(o instanceof Personne){
			Personne p = (Personne)o;

			/*System.out.println(this.nom + " & " + p.getNom() + " = " + (this.nom.equals(p.getNom())));
			System.out.println(this.prenom + " & " + p.getPrenom() + " = " + (this.prenom.equals(p.getPrenom())));
			System.out.println(this.sexe + " & " + p.getSexe() + " = " + (this.sexe == p.getSexe()));
			System.out.println(this.pro + " & " + p.getPro() + " = " + (this.pro == p.getPro()));
			System.out.println(this.numeros + " & " + p.getNumeros() + " = " + (this.numeros.equals(p.getNumeros())));*/

			boolean defaut = (this.nom.equals(p.getNom()) && this.prenom.equals(p.getPrenom()) && this.pro == p.getPro() && this.numeros.equals(p.getNumeros()) && this.adresses.equals(p.getAdresses()) && this.emails.equals(p.getEmails()));
	

			return ((defaut && this.sexe == p.getSexe()) || (defaut && this.nom_societe.equals(p.getNomSociete())));
		} else {
			return false;
		}
	}
	

	/**
	  * Renseigne le nom de la société de la personne
	  *
	  * @param noms le nom de la société de la personne
	  */
	public void renseignerNomSociete(String noms){
		this.nom_societe = noms;
	}

	/**
	  * Renseigne les numeros de la personne
	  *
	  * @param numeros les numeros de la personne
	  */
	public void renseignerNumeros(LinkedHashMap<String, String> numeros){
		this.numeros = numeros;
	}

	/**
	  * Renseigne les adresses de la personne
	  *
	  * @param adresses les adresses de la personne
	  */
	public void renseignerAdresses(LinkedHashMap<String, String> adresses){
		this.adresses = adresses;
	}

	/**
	  * Renseigne les emails de la personne
	  *
	  * @param emails les emails de la personne
	  */
	public void renseignerEmails(LinkedHashMap<String, String> emails){
		this.emails = emails;
	}

	/**
	  * Ajoute un numero à la personne
	  *
	  * @param libele le libele du numero
	  * @param numero le numero
	  */
	public void ajouterNumero(String libele, String numero) throws DoublonException{
		if(!numeros.containsKey(libele)){
			this.numeros.put(libele, numero);
		} else {
			throw new DoublonException("Il existe déjà un numéro avec ce libélé");
		}
	}

	/**
	  * Ajoute une adresse à la personne
	  *
	  * @param libele le libele de l'adresse
	  * @param adresse l'adresse
	  */
	public void ajouterAdresse(String libele, String adresse) throws DoublonException{
		if(!adresses.containsKey(libele)){
			this.adresses.put(libele, adresse);
		} else {
			throw new DoublonException("Il existe déjà une adresse avec ce libélé");
		}
	}

	/**
	  * Ajoute une adresse email à la personne
	  *
	  * @param libele le libele de l'adresse email
	  * @param email l'adresse email
	  */
	public void ajouterEmail(String libele, String email) throws DoublonException{
		if(!emails.containsKey(libele)){
			this.emails.put(libele, email);
		} else {
			throw new DoublonException("Il existe déjà un email avec ce libélé");
		}
	}
	
	/**
	  * Renseigne le prénom
	  *
	  * @return le prénom
	  */
	public String getPrenom(){
		return this.prenom;
	}

	/**
	  * Renseigne le nom de la société
	  *
	  * @return le nom de la société
	  */
	public String getNomSociete(){
		return this.nom_societe;
	}

	/**
	  * Renseigne le nom
	  *
	  * @return le nom
	  */
	public String getNom(){
		return this.nom;
	}

	/**
	  * Renseigne le sexe de la personne
	  *
	  * @return le sexe de la personne
	  */
	public char getSexe(){
		return this.sexe;
	}

	/**
	  * Renseigne si le contact est professionel
	  *
	  * @return si le contact est professionel
	  */
	public boolean getPro(){
		return this.pro;
	}

	/**
	  * Renseigne les numéros de la personne
	  *
	  * @return les numéros de la personne
	  */
	public LinkedHashMap<String, String> getNumeros(){
		return this.numeros;
	}

	/**
	  * Renseigne les adresses de la personne
	  *
	  * @return les adresses de la personne
	  */
	public LinkedHashMap<String, String> getAdresses(){
		return this.adresses;
	}

	/**
	  * Renseigne les adresses e-mail de la personne
	  *
	  * @return les adresses e-mail de la personne
	  */
	public LinkedHashMap<String, String> getEmails(){
		return this.emails;
	}

	/**
	  * Renseigne l'identifiant de la personne
	  *
	  * @return l'identifiant de la personne
	  */
	public int getID(){
		return this.ID;
	}

	/**
	  * Renseigne l'hashcode de la personne
	  *
	  * @return l'hashcode
	  */
	public int hashCode(){
		return this.nom.hashCode()*this.prenom.hashCode();
	}

	/**
	  * Compare deux personnes
	  *
	  * @param p la personne a comparer
	  *
	  * @return le résultat de la comparaison
	  */
	public int compareTo(Personne p){
		char[] nom_p = p.getNom().toCharArray();
		char[] nom_this = this.nom.toCharArray();

		char[] prenom_p = p.getPrenom().toCharArray();
		char[] prenom_this = this.prenom.toCharArray();

		int resultat = 0;

		if(nom_p != nom_this){
			// On cherche à comparer deux personnes par leur nom de famille
			for(int i = 0; i < Math.min(nom_p.length, nom_this.length); i++){
				if(nom_p[i] < nom_this[i]){
					resultat = 1;
					break;
				} else if(nom_p[i] > nom_this[i]){
					resultat = -1;
					break;
				}
			}	
		} else {
			// Si elles ont le même nom de famille on différencie par le prénom
			for(int i = 0; i < Math.min(prenom_p.length, prenom_this.length); i++){
				if(prenom_p[i] < prenom_this[i]){
					resultat = 1;
					break;
				} else if(prenom_p[i] > prenom_this[i]){
					resultat = -1;
					break;
				}
			}
		}

		return resultat;
	}

	public String toString(){
		return this.nom + " " + this.prenom;
	}
}