package vue;

import modele.*;
import controleurs.*;

import javax.swing.*;
import java.util.*;
import java.util.regex.*;
import java.awt.*;

/**
  * Classe Groupe : sert à regrouper les champs extensibles comme les numéros, adresses et adresses e-mail
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Groupe<K, V> extends JPanel{
	/**
	  * Stocke la collection de champs passés en paramètre sous le format de champs de texte
	  */
	private LinkedHashMap<ModifChamp, ModifChamp> collectionChamps;

	/**
	  * Stocke la collection de champs passés en paramêtre
	  */
	private LinkedHashMap<K, V> collection;

	/**
	  * Nom du groupe à afficher
	  */
	private String nom;

	/**
	  * Le bouton qui sert à ajouter un nouveau numéro
	  */
	private JButton bouton_ajouter;

	private Pattern pattern;
	private static Pattern pattern_label = Pattern.compile("(([a-zA-Z]{1,20}[0-9]{0,2}))?");

	private JPanel pan_center;

	/**
	  * Constructeur du groupe, initialise la collection de champs et affiche ceux-ci
	  *
	  * @param collection la liste des champs
	  * @param nom le nom du groupe à afficher
	  */
	public Groupe(LinkedHashMap<K,V> collection, String nom, Pattern pattern){
		super();

		// Initialisation du groupe
		this.nom = nom;
		this.collection = collection;
		this.pattern = pattern;
		initialiserGroupe();

		pan_center = new JPanel();
		pan_center.setLayout(new BoxLayout(pan_center, BoxLayout.Y_AXIS));

		Iterator<ModifChamp> it = collectionChamps.keySet().iterator();

		while(it.hasNext()){
			ModifChamp cle = it.next();
			cle.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");
			ModifChamp valeur = (ModifChamp)collectionChamps.get(cle);
			valeur.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");

			JPanel pan = new JPanel();
			pan.setLayout(new FlowLayout(FlowLayout.LEFT));

			pan.add(cle);
			pan.add(valeur);
			pan_center.add(pan);
		}

		JPanel pan_south = new JPanel();
		pan_south.setLayout(new FlowLayout(FlowLayout.CENTER));
		pan_south.add(this.bouton_ajouter);
		//pan_south.add(this.bouton_supprimer);

		this.setLayout(new BorderLayout());
		this.add(pan_center, BorderLayout.CENTER);
		this.add(pan_south, BorderLayout.SOUTH);
		
		this.setBorder(BorderFactory.createTitledBorder(nom));
	}

	/**
	  * Permet de recharger le groupe, les informations du modele
	  */
	public void rechargerGroupe(){
		Iterator<K> it = collection.keySet().iterator();
		Iterator<ModifChamp> it2 = collectionChamps.keySet().iterator();

		while(it.hasNext() && it2.hasNext()){
			K cle = it.next();
			V valeur = collection.get(cle);

			ModifChamp label = it2.next();
			label.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");
			ModifChamp value = collectionChamps.get(label);
			value.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");

			label.setText((String)cle);
			value.setText((String)valeur);
		}

		for(Iterator<ModifChamp> it3 = collectionChamps.keySet().iterator(); it3.hasNext(); ){
			ModifChamp cle = it3.next();
			ModifChamp valeur = collectionChamps.get(cle);

			JPanel pan = new JPanel();
			pan.setLayout(new FlowLayout(FlowLayout.LEFT));

			pan.add(cle);
			pan.add(valeur);

			pan_center.remove(pan);

			if(cle.getText().equals(""))
				it3.remove();
		}
	}

	/**
	  * Récupère l'expression régulière du groupe
	  *
	  * @return le pattern du groupe
	  */
	public Pattern getPattern(){
		return this.pattern;
	}

	/**
	  * Initialise le groupe
	  */
	private void initialiserGroupe(){
		this.collectionChamps = new LinkedHashMap<ModifChamp, ModifChamp>();

		for(Iterator<K> it = collection.keySet().iterator(); it.hasNext(); ){
			K cle = it.next();
			V valeur = collection.get(cle);

			this.collectionChamps.put(new ModifChamp((String)cle), new ModifChamp(valeur.toString()));
		}

		this.bouton_ajouter = new JButton("+");
		this.bouton_ajouter.addActionListener(new AjouterElementControleur(this));
		this.bouton_ajouter.setVisible(false);

		//this.bouton_supprimer = new JButton("-");
		//this.boutons_supprimer.addActionListener();
		//this.bouton_supprimer.setVisible(false);
	}

	/**
	  * Permet d'ajouter un champ dans le groupe
	  */
	public void ajouterElement(){
		JPanel pan = new JPanel();
		pan.setLayout(new FlowLayout(FlowLayout.LEFT));

		ModifChamp mc = new ModifChamp("");
		mc.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");
		ModifChamp mv = new ModifChamp("");
		mv.setToolTipText("Voir plus d'informations sur la validation des champs dans le menu d'aide");

		pan.add(mc);
		pan.add(mv);

		collectionChamps.put(mc, mv);

		pan_center.add(pan);
		pan_center.repaint();
	}

	/**
	  * Active ou désactive la modification des champs du groupe
	  *
	  * @param modif l'activation de la modification
	  */
	public void activerModif(boolean modif){
		for(Iterator<ModifChamp> it = collectionChamps.keySet().iterator(); it.hasNext(); ){
			ModifChamp cle = it.next();
			ModifChamp valeur = collectionChamps.get(cle);

			cle.activerModif(modif);
			valeur.activerModif(modif);
		}

		this.bouton_ajouter.setVisible(modif);
		//this.bouton_supprimer.setVisible(modif);
	}

	/**
	  * Retourne la collection de champs de texte en collection de base
	  *
	  * @return la collection de base
	  */
	public LinkedHashMap<String, String> getCollection(){
		LinkedHashMap<String, String> c = new LinkedHashMap<String, String>();

		for(Iterator<ModifChamp> it = collectionChamps.keySet().iterator(); it.hasNext(); ){
			ModifChamp cle = it.next();
			ModifChamp valeur = (ModifChamp)collectionChamps.get(cle);
			
			if(!cle.getText().equals(""))
				c.put(cle.getText(), valeur.getText());
		}

		return c;
	}

	/**
	  * Dit si le champ est vide ou pas
	  *
	  * @return si le champ est vide ou pas
	  */
	public boolean champVide(){
		boolean test = false;

		for(Iterator<ModifChamp> it = this.collectionChamps.keySet().iterator(); it.hasNext(); ){
			ModifChamp cle = it.next();

			if(cle.getText().equals("")){
				test = true;
				break;
			}
		}

		return test;
	}

	/**
	  * Applique les expressions régulières sur les champs du groupe
	  *
	  * @return le resultat de l'application des patterns
	  */
	public boolean matchPatternFields(){
		boolean result = true;

		for(Iterator<ModifChamp> it = collectionChamps.keySet().iterator(); it.hasNext(); ){
			ModifChamp cle = it.next();
			ModifChamp valeur = collectionChamps.get(cle);

			Matcher m_c = pattern_label.matcher(cle.getText());
			Matcher m_v = this.pattern.matcher(valeur.getText());

			if(!m_c.matches() || !m_v.matches()){
				result = false;
				break;
			}
		}

		return result;
	}
}