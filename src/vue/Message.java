package vue;

import javax.swing.JOptionPane;

/**
  * Classe message, sert à afficher différents types de boites de dialogues
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Message{
	private static JOptionPane jop;

	/**
	  * Constructeur de la classe message, initialise le JOptionPane
	  */
	public Message(){
		jop = new JOptionPane();
	}

	/**
	  * Affiche un message préventif et questionne l'utilisateur
	  *
	  * @param title le titre du message
	  * @param message le message a afficher
	  *
	  * @return si l'utilisateur à appuyé sur OK
	  */
	public static boolean afficherMessage(String title, String message){
		int option = jop.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	
		return (option == JOptionPane.OK_OPTION);
	}

	/**
	  * Affiche un message préventif
	  *
	  * @param title le titre du message
	  * @param message le message a afficher
	  */
	public static void afficherAvertissement(String title, String message){
		jop.showMessageDialog(null, message, title, JOptionPane.WARNING_MESSAGE);
	}

	/**
	  * Affiche un message et questionne l'utilisateur avec duex choix
	  *
	  * @param title le titre du message
	  * @param message le message a afficher
	  *
	  * @return le choix de l'utilisateur
	  */
	public static String demanderProfessionel(String title, String message){
		String[] type = {"Professionel", "Personnel"};

		int indice = jop.showOptionDialog(null, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, type, type[1]);

		return type[indice];
	}

	/**
	  * Affiche un message informatif
	  *
	  * @param title le titre du message
	  * @param message le message a afficher
	  */
	public static void afficherMessageInfo(String title, String message){
		jop.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
	}
}