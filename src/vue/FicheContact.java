package vue;

import modele.*;
import controleurs.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import java.util.regex.*;

/**
  * Classe FicheContact : permet l'affichage et la modification d'un contact du carnet d'adresse
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class FicheContact extends JPanel{
	/**
	  * Atributs fonctionels
	  */
	private Personne personne;
	private int id;
	private Carnet modele;
	private Fenetre parent;

	/**
	  * Panels de stockage
	  */
	private JPanel panneau_centre;
	private JPanel panneau_sud;
	private JPanel panneau_test;
	private JPanel panneau_nom, panneau_prenom, panneau_sexe, panneau_nom_societe;

	/**
	  * Champs par défaut
	  */
	private JLabel label_nom, label_prenom, label_sexe;
	private ModifChamp valeur_nom, valeur_prenom;
	private JComboBox valeur_sexe;

	/**
	  * Champs pour professionel - le sexe
	  */
	private JLabel label_societe;
	private ModifChamp valeur_societe;

	/**
	  * Vector pour le choix entre homme et femme (sexe)
	  */
	private static Vector<String> label_combobox_sexe;

	/**
	  * Boutons
	  */
	private JButton bouton_modification;
	private JButton bouton_suppression;

	/**
	  * Champs extensibles
	  */
	private Groupe<String, String> groupe_numeros, groupe_emails, groupe_adresses;

	private Pattern p_numeros;
	private Pattern p_adresses;
	private Pattern p_emails;

	/**
	  * Indique si la fiche de contact est en modification ou pas
	  */
	private static boolean IS_IN_MODIFICATION = false;

	private static boolean IS_PRO;

	/**
	  * Constructeur de la fiche de contact, initialise et affiche les informations d'une personne
	  *
	  * @param id l'id de la personne à afficher
	  * @param modele le modele de l'application
	  * @param parent l'element parent de la fiche de contact, à savoir la vue/fenêtre principale
	  */
	public FicheContact(int id, Carnet modele, Fenetre parent){
		super();

		this.modele = modele;
		this.id = id;
		this.personne = this.modele.getPersonne(id);
		IS_PRO = this.personne.getPro();
		this.parent = parent;

		this.setPreferredSize(new Dimension(300, 200));

		this.p_numeros = this.modele.getPatternNumeros();
		this.p_adresses = this.modele.getPatternAdresses();
		this.p_emails = this.modele.getPatternEmails();

		label_combobox_sexe = new Vector<String>();
		label_combobox_sexe.add("Homme");
		label_combobox_sexe.add("Femme");

		this.panneau_centre = new JPanel();
		this.panneau_centre.setLayout(new BoxLayout(panneau_centre, BoxLayout.Y_AXIS));

		construireBoutons();
		construireChampLV();
		construirePaneaux();
		ajouterChamps();

		activerModification(false);

		this.setLayout(new BorderLayout());

		this.add(this.panneau_test, BorderLayout.NORTH);
		this.add(this.panneau_centre, BorderLayout.CENTER);
		this.add(this.panneau_sud, BorderLayout.SOUTH);
	}

	/**
	  * Recharge l'ancien contact dans la fiche contact
	  */
	public void rechargerContact(){
		this.valeur_nom.setText(personne.getNom());
		this.valeur_prenom.setText(personne.getPrenom());

		this.valeur_sexe.setSelectedItem(new String((this.personne.getSexe() == 'M') ? "Homme" : "Femme"));
		
		if(IS_PRO)
			this.valeur_societe.setText(personne.getNomSociete());

		/*this.groupe_numeros.rechargerGroupe();
		this.groupe_adresses.rechargerGroupe();
		this.groupe_emails.rechargerGroupe();*/

		this.panneau_centre.remove(this.groupe_numeros);
		this.panneau_centre.remove(this.groupe_adresses);
		this.panneau_centre.remove(this.groupe_emails);

		this.groupe_numeros = new Groupe<String, String>(this.personne.getNumeros(), "Numeros", p_numeros);
		this.groupe_adresses = new Groupe<String, String>(this.personne.getAdresses(), "Adresses", p_adresses);
		this.groupe_emails = new Groupe<String, String>(this.personne.getEmails(), "Adresses e-mail", p_emails);

		this.panneau_centre.add(this.groupe_numeros);
		this.panneau_centre.add(this.groupe_adresses);
		this.panneau_centre.add(this.groupe_emails);

		this.repaint();
	}

	/**
	  * Met à jour la personne dans la fiche de contact
	  */
	public void updatePersonne(){
		this.personne = this.modele.getPersonne(this.id);
	}

	/**
	  * Indique si le contact est à jour en fonction du modèle
	  */
	public boolean contactAJour(){
		return this.getPersonne().equals(this.personne);
	}

	/**
	  * Permet de récupérer un objet Personne avec les informations qui sont affichées dans la fiche de contact
	  *
	  * @return Personne la personne affichée dans la fiche de contact
	  */
	public Personne getPersonne(){
		Personne p = new Personne(this.valeur_nom.getText(), this.valeur_prenom.getText(), (this.valeur_sexe.getSelectedItem() == "Homme" ? 'M' : 'F'), IS_PRO);
		p.renseignerNumeros(this.groupe_numeros.getCollection());
		p.renseignerAdresses(this.groupe_adresses.getCollection());
		p.renseignerEmails(this.groupe_emails.getCollection());

		if(IS_PRO)
			p.renseignerNomSociete(this.valeur_societe.getText());

		return p;
	}

	/**
	  * Permet d'activer ou de désactiver la modification des champs de la fiche de contact
	  *
	  * @param modif indique si les champs sont modifiables ou pas
	  */
	public void activerModification(boolean modif){
		IS_IN_MODIFICATION = modif;

		// On active la modification des champs nom et prénom...
		//Image icon = new ImageIO().read(getClass().getResource("images/13.png"));
		this.valeur_nom.activerModif(modif);
		this.valeur_prenom.activerModif(modif);

		// ... puis celle du champ sexe, qui est un combobox et n'a pas de méthode par défaut...
		this.valeur_sexe.setEnabled(modif);
		this.valeur_sexe.setOpaque(modif);
		this.valeur_sexe.setFont(new Font("Arial", Font.PLAIN, 12));
		
		if(IS_PRO)
			this.valeur_societe.activerModif(modif);


		// ... et enfin celle des groupes numéros, adresses et email
		this.groupe_numeros.activerModif(modif);
		this.groupe_adresses.activerModif(modif);
		this.groupe_emails.activerModif(modif);

		// D'une vue à une autre les boutons ne servent pas à la même chose
		if(modif){
			// Si la modification est activée on change les boutons pour pouvoir valider les modifications ou bien annuler
			//this.bouton_modification.setText("Valider");
			this.bouton_modification.setIcon(new ImageIcon("images/5.png"));
			this.bouton_suppression.setIcon(new ImageIcon("images/4.png"));

			// On suipprime tous les controleurs associés aux boutons...
			for(java.awt.event.ActionListener controleur : this.bouton_modification.getActionListeners())
				this.bouton_modification.removeActionListener(controleur);

			for(java.awt.event.ActionListener controleur : this.bouton_suppression.getActionListeners())
				this.bouton_suppression.removeActionListener(controleur);

			// ... puis on rajoute les nouveaux
			this.bouton_modification.addActionListener(new ValiderControleur(this.parent, this.modele));
			this.bouton_suppression.addActionListener(new AnnulerControleur(this.parent, this.modele));
		} else {
			// Si la modification n'est pas activée on change les boutons afin de permetre de modifier ou supprimer le contact
			//this.bouton_modification.setText("Modifier");
			this.bouton_modification.setIcon(new ImageIcon("images/1.png"));
			this.bouton_suppression.setIcon(new ImageIcon("images/2.png"));

			for(java.awt.event.ActionListener controleur : this.bouton_modification.getActionListeners())
				this.bouton_modification.removeActionListener(controleur);

			for(java.awt.event.ActionListener controleur : this.bouton_suppression.getActionListeners())
				this.bouton_suppression.removeActionListener(controleur);

			this.bouton_modification.addActionListener(new ModifierControleur(this.parent, this.modele, true));
			this.bouton_suppression.addActionListener(new SupprimerContactControleur(this.parent, this.modele));
		}
	}

	/**
	  * Indique si la fiche de contact est en modifications
	  *
	  * @return si la fiche de contact est en modification ou pas
	  */
	public static boolean isInModification(){
		return IS_IN_MODIFICATION;
	}

	/**
	  * On construit les boutons
	  */
	private void construireBoutons(){
		this.bouton_suppression = new JButton("");
		//JButton square = new JButton(new ImageIcon("images/carré.jpg")),
		this.bouton_modification = new JButton("");
		this.bouton_modification.addActionListener(new ModifierControleur(this.parent, this.modele, true));
	}

	/**
	  * On construit les champs affichés dans la fiche de contact
	  */
	private void construireChampLV(){
		this.label_nom = new JLabel("Nom :");
		this.label_prenom = new JLabel("Prénom :");

		
		this.label_sexe = new JLabel("Sexe :");

		if(IS_PRO)
			this.label_societe = new JLabel("Nom société :");

		this.valeur_nom = new ModifChamp(personne.getNom());
		this.valeur_nom.setToolTipText("Doit commencer par une majuscule");
		this.valeur_prenom = new ModifChamp(personne.getPrenom());
		this.valeur_prenom.setToolTipText("Doit commencer par une majuscule");

		
		this.valeur_sexe = new JComboBox(label_combobox_sexe);
		this.valeur_sexe.setSelectedItem(new String((this.personne.getSexe() == 'M') ? "Homme" : "Femme"));

		if(IS_PRO){
			this.valeur_societe = new ModifChamp(personne.getNomSociete());
			this.valeur_societe.setToolTipText("Doit commencer par une majuscule");
		}

		this.groupe_numeros = new Groupe<String, String>(this.personne.getNumeros(), "Numeros", p_numeros);
		this.groupe_adresses = new Groupe<String, String>(this.personne.getAdresses(), "Adresses", p_adresses);
		this.groupe_emails = new Groupe<String, String>(this.personne.getEmails(), "Adresses e-mail", p_emails);
	}

	/**
	  * On construit tous les panneaux necessaire à l'affichage des champs
	  */
	private void construirePaneaux(){
		this.panneau_nom = new JPanel();
		this.panneau_prenom = new JPanel();
		this.panneau_sexe = new JPanel();
		this.panneau_sud = new JPanel();
		this.panneau_nom_societe = new JPanel();

		this.panneau_nom.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.panneau_prenom.setLayout(new FlowLayout(FlowLayout.LEFT));

		this.panneau_sexe.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.panneau_nom_societe.setLayout(new FlowLayout(FlowLayout.LEFT));

		this.panneau_sud.setLayout(new FlowLayout(FlowLayout.RIGHT));
	}

	/**
	  * On les ajoutes aux panneaux puis à la fiche de contact
	  */
	private void ajouterChamps(){
		this.panneau_test = new JPanel();
		this.panneau_test.setLayout(new BorderLayout());

		JPanel panneau_test_east = new JPanel();
		panneau_test_east.setLayout(new BoxLayout(panneau_test_east, BoxLayout.Y_AXIS));

		JPanel panneau_test_west = new JPanel();
		panneau_test_west.setLayout(new BoxLayout(panneau_test_west, BoxLayout.Y_AXIS));

		panneau_nom.add(label_nom);
		panneau_prenom.add(label_prenom);
		
		panneau_nom.add(valeur_nom);
		panneau_prenom.add(valeur_prenom);

		panneau_sud.add(bouton_modification);
		panneau_sud.add(bouton_suppression);

		panneau_test_east.add(panneau_nom);
		panneau_test_east.add(panneau_prenom);

		
		panneau_sexe.add(label_sexe);
		panneau_sexe.add(valeur_sexe);

		if(IS_PRO){
			panneau_nom_societe.add(label_societe);
			panneau_nom_societe.add(valeur_societe);
		}

		panneau_test_east.add(panneau_sexe);

		if(IS_PRO)
			panneau_test_east.add(panneau_nom_societe);
		
		this.panneau_centre.add(this.groupe_numeros);
		this.panneau_centre.add(this.groupe_adresses);
		this.panneau_centre.add(this.groupe_emails);

		if(this.personne.getSexe() == 'M')
			panneau_test_west.add(new JLabel(new ImageIcon("images/7.png")));
		else
			panneau_test_west.add(new JLabel(new ImageIcon("images/6.png")));
		

		this.panneau_test.add(panneau_test_east, BorderLayout.CENTER);
		this.panneau_test.add(panneau_test_west, BorderLayout.WEST);
	}

	/**
	  * Retourne l'identifiant de la personne de la fiche de contact
	  *
	  * @return l'identifiant de la personne en cours
	  */
	public int getID(){
		return this.id;
	}

	public boolean matchPatternFields(){
		boolean result = false;

		Pattern p_nom_prenom_societe = Pattern.compile("[A-Z][a-zA-Z]{1,30}");

		Matcher m_nom = p_nom_prenom_societe.matcher(this.valeur_nom.getText());
		Matcher m_prenom = p_nom_prenom_societe.matcher(this.valeur_prenom.getText());

		Matcher m_societe = null;
		
		if(IS_PRO)
			m_societe = p_nom_prenom_societe.matcher(this.valeur_societe.getText());

		if(!IS_PRO)
			result = m_nom.matches() && m_prenom.matches() && this.groupe_numeros.matchPatternFields() && this.groupe_emails.matchPatternFields() && this.groupe_adresses.matchPatternFields();
		else 
			result = m_nom.matches() && m_prenom.matches() && m_societe.matches() && this.groupe_numeros.matchPatternFields() && this.groupe_emails.matchPatternFields() && this.groupe_adresses.matchPatternFields();

		return result;
	}
}