package vue;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import modele.*;
import controleurs.*;

import java.awt.event.*;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;

/**
  * Classe Fenetre : Classe principale de notre vue, qui va créer notre fenêtre
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Fenetre extends JFrame{
	private Carnet modele;

	private JPanel panneau_barre_statut, panneau_nord;
	private Tab panelOngl, panelOngl1;
	private JTabbedPane onglet, proPers;
	private JMenuBar menuBar;
	private JMenu fichier, edition, aide;
	private JLabel text;

	private FicheContact fc;

	private char monTableau[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	private String monTableau2[] = {"A - E", "F - J", "K - O", "P - T", "U - Z", "FAMILLE", "AMIS", "COLLEGUE"};

	public int idEnCours;
	

	/**
	 * Constructeur de notre fenêtre
	 * @param  title Titre de la fenêtre
	 */
	public Fenetre(String title){
		super(title);

		// Propriétés de la fenêtre
		this.setSize(588, 619);
		this.setMinimumSize(new Dimension(588,619));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);


		// Initialisation du modèle
		SauvegardeCarnet sc = new SauvegardeCarnet(null);
		this.modele = sc.restauration();

		//this.modele = new Carnet();
		//this.modele.remplir();

		this.addWindowListener(new FenetreControleur(this, this.modele));

		// Centre - fiche de contact
		idEnCours = 7;
		fc = new FicheContact(idEnCours, this.modele, this);

		contactOnglet();
		barreStatut();
		panneauNorth();
		construireMenus();

		this.setLayout(new BorderLayout(10, 10));

		this.setJMenuBar(this.menuBar);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("images/10.png"));
		this.getContentPane().add(onglet, BorderLayout.EAST);
		this.getContentPane().add(panneau_barre_statut, BorderLayout.SOUTH);
		this.getContentPane().add(panneau_nord, BorderLayout.NORTH);
		this.getContentPane().add(this.fc, BorderLayout.CENTER);

		try { 
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setVisible(true);
	}

	/**
	 * Met a jour les onglets de personnes (suite à un ajout/une suppression)
	 */
	public void rafraichirOnglets(){
		this.getContentPane().remove(onglet);
		contactOnglet();
		this.getContentPane().add(onglet, BorderLayout.EAST);
		this.revalidate();
	}

	/**
	 * Construit le Menu de la fenêtre (en haut)
	 */
	public void construireMenus(){
		this.menuBar = new JMenuBar();

		this.edition = new JMenu("Edition");
		this.aide = new JMenu("Aide");

		JMenuItem nouveau = new JMenuItem("Nouveau Contact");
		nouveau.addActionListener(new AjouterContactControleur(this, this.modele));
		JMenuItem modifier = new JMenuItem("Modifier Contact");
		modifier.addActionListener(new ModifierControleur(this, this.modele, true));
		JMenuItem supprimer = new JMenuItem("Supprimer Contact");
		supprimer.addActionListener(new SupprimerContactControleur(this, this.modele));

		this.edition.add(nouveau);
		this.edition.add(modifier);
		this.edition.add(supprimer);

		JMenuItem readme = new JMenuItem("Readme");
		readme.addActionListener(new ReadMeControleur(this, this.modele));

		this.aide.add(readme);

		this.menuBar.add(edition);
		this.menuBar.add(aide);
	}

	/**
	 * Change l'affichage de la fiche de contact
	 * @param id ID de la personne affichée
	 */
	public void changeContact(int id){
		this.idEnCours = id;
		this.getContentPane().remove(this.fc);
		this.fc = null;
		this.fc = new FicheContact(idEnCours, this.modele, this);
		this.getContentPane().add(this.fc, BorderLayout.CENTER);
	}

	/**
	 * Récupère l'ID de la personne qui est affichée
	 * @return L'ID voulu
	 */
	public int getID(){
		return this.idEnCours;
	}

	/**
	 * Récupère la fiche de contact sélectionnée
	 * @return La fiche contact
	 */
	public FicheContact getFicheContact(){
		return this.fc;
	}

	/**
	 * Fabrique la barre de statut
	 */
	public void barreStatut(){
		this.panneau_barre_statut = new JPanel();
		this.panneau_barre_statut.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.panneau_barre_statut.setBorder(BorderFactory.createEtchedBorder());

		this.text = new JLabel("Nombre de contacts : "+this.modele.getNbPersonnes());

		this.panneau_barre_statut.add(text);
	}

	/**
	 * Met à jour la barre de statut
	 */
	public void refreshStatut(){
		this.panneau_barre_statut.remove(text);
		text = new JLabel("Nombre de contacts : "+this.modele.getNbPersonnes());
		this.panneau_barre_statut.add(text);
		this.panneau_barre_statut.revalidate();
	}

	/**
	 * Fabrique la partie nord de notre fenêtre 
	 */
	public void panneauNorth(){
		this.panneau_nord = new JPanel();
		this.panneau_nord.setLayout(new BorderLayout());
		this.panneau_nord.setBorder(BorderFactory.createEtchedBorder());

		JLabel titre = new JLabel("Mon carnet d'adresses");
		Font police = new Font("Tahoma", Font.BOLD, 20);
		titre.setFont(police);

		JButton ajout = new JButton(new ImageIcon("images/3.png"));
		ajout.addActionListener(new AjouterContactControleur(this, this.modele));

		this.panneau_nord.add(titre, BorderLayout.CENTER);
		this.panneau_nord.add(ajout, BorderLayout.EAST);
	}

	/**
	 * Fabrique la partie droite de la fenêtre (tout les onglets)
	 */
	public void contactOnglet(){
		this.onglet = new JTabbedPane(JTabbedPane.RIGHT);
		this.onglet.setBorder(BorderFactory.createEtchedBorder());

		for(int i = 0 ; i < 26 ; i++)
		{
			createPage(monTableau[i]);
		}

	}

	/**
	 * Fabrique un onglet 
	 * @param lettre La lettre de l'onglet
	 */
	public void createPage(char lettre){
		this.proPers = new JTabbedPane(JTabbedPane.TOP);

		panelOngl = new Tab(modele.sousListe(lettre, true), this.modele, this);
		panelOngl.setPreferredSize(new Dimension(10,100));


		panelOngl1 = new Tab(modele.sousListe(lettre, false), this.modele, this);
		panelOngl1.setPreferredSize(new Dimension(10,100));


		this.proPers.addTab("Professionnel", panelOngl);
		this.proPers.addTab("Personnel", panelOngl1);


		this.onglet.addTab(lettre + "",proPers);
	}
}