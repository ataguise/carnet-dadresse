package vue;

import modele.*;
import controleurs.*;

import java.awt.*;
import javax.swing.*;
import java.util.*;

/**
  * Classe Tab : permet l'affichage de tous les onglets de tri des contacts dans le carnet d'adresse
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class Tab extends JPanel{
	private JScrollPane maBar;
	private Carnet modele;
	private Fenetre vue;
	private JList listeContact;

	private int i = 0;

	private ArrayList<Personne> arp;
	private ArrayList<String> listCop;
	
	/**
	 * Constructeur d'un onglet
	 * @param  arp    La liste de personne que l'onglet doit afficher
	 * @param  modele Notre modele
	 * @param  vue    Notre vue
	 */
	public Tab(ArrayList<Personne> arp, Carnet modele, Fenetre vue){
		this.vue = vue;
		this.modele = modele;
		this.arp = arp;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.listeContact = new JList();

		DefaultListModel modelList = new DefaultListModel();
        for(Iterator<Personne> it = arp.iterator(); it.hasNext();){
         	Personne p = it.next();
            modelList.addElement(p.toString());      
        }

        this.listeContact.setModel(modelList); 
        this.listeContact.setBackground(new Color(220, 220, 220));

		this.listeContact.addListSelectionListener(new TabControleur(modele, vue, this));
		this.maBar = new JScrollPane(listeContact);
		this.add(maBar); 
	}

	/**
	 * Permet de récupérer le contact sélectionner
	 * @return Nom-Prenom du contact
	 */
	public String getValueSelected(){
		return (String)this.listeContact.getSelectedValue();
	
	}
}