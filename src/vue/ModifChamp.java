package vue;

import javax.swing.*;
import java.awt.*;

/**
  * Classe ModifChamp : redéfini un JTextField
  *
  * @version 1.0
  * @author BOUSCHARAIN Jonas, DAILLAND, Arthur et DEFOIS Paul
  */
public class ModifChamp extends JTextField{
	public ModifChamp(String str){
		super(str);
		this.setColumns(10);
		this.setDisabledTextColor(Color.BLACK);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}

	/**
	  * permet d'activer les champs pour la modification
	  *
	  * @param modif si les champs doivent être modifiable ou pas
	  */
	public void activerModif(boolean modif){
		this.setEnabled(modif);
		this.setOpaque(modif);
		this.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		this.setFont(new Font("Arial", Font.PLAIN, 12));
	}

	public String getText(){
		return super.getText();
	}
}