package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import modele.*;
import vue.*;

 
public class AjouterElementControleur implements ActionListener{
	private Groupe grp;

	public AjouterElementControleur(Groupe grp){
		this.grp = grp;
	}

	public void actionPerformed(ActionEvent event){
		Message m = new Message();

		if(!this.grp.champVide()){
			this.grp.ajouterElement();
			this.grp.revalidate();
			this.grp.repaint();
		} else {
			m.afficherAvertissement("Attention", "Remplissez le champ vide avant d'en ajouter un autre.");
		}
	}
}