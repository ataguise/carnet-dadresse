package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

import modele.*;
import vue.*;

public class AnnulerControleur extends Controleur{
	public AnnulerControleur(Fenetre vue, Carnet modele){
		super(vue, modele);
	}

	public void actionPerformed(ActionEvent event){
		Message m = new Message();

		// Récrire dans les champs du contact, les anciennes valeur
		if(!super.vue.getFicheContact().contactAJour()){
			if(m.afficherMessage("Attention", "Les modifications ne serons pas enregistrées, voulez vous continuer ?")){
				super.vue.getFicheContact().rechargerContact();
				super.vue.getFicheContact().activerModification(false);
				super.vue.repaint();
			}
		} else {
			super.vue.getFicheContact().rechargerContact();
			super.vue.getFicheContact().activerModification(false);
			super.vue.repaint();
		}
	}
}