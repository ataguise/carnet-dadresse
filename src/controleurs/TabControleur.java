package controleurs;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.*;

import modele.*;
import vue.*;

public class TabControleur implements ListSelectionListener{
	private Carnet modele;
	private Fenetre vue;
	private Tab t;

	public TabControleur(Carnet modele, Fenetre vue, Tab t){
		this.modele = modele;
		this.vue = vue;
		this.t = t;
	}

	public void valueChanged(ListSelectionEvent e){
		Message m = new Message();

		if(this.vue.getFicheContact().isInModification()){
			if(m.afficherMessage("Attention", "Les modifications ne serons pas enregistrées, voulez vous continuer ?")){
				int i = this.modele.getTabasco().get(this.t.getValueSelected());
				vue.changeContact(i);
				vue.repaint();
			}
		} else {
			int i = this.modele.getTabasco().get(this.t.getValueSelected());
			vue.changeContact(i);
			vue.repaint();
		}
	}
}