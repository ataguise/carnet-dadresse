package controleurs;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

import modele.*;
import vue.*;

public class FenetreControleur implements WindowListener{
	private Carnet modele;
	private Fenetre vue;

	public FenetreControleur(Fenetre vue, Carnet modele){
		this.vue = vue;
		this.modele = modele;
	}

	public void windowActivated(WindowEvent e){}
	public void windowClosed(WindowEvent e){}
	public void windowClosing(WindowEvent e){
		SauvegardeCarnet sc = new SauvegardeCarnet(this.modele);

		sc.sauvegarde();
	}
	public void windowDeactivated(WindowEvent e){}
	public void windowDeiconified(WindowEvent e){}
	public void windowIconified(WindowEvent e){}
	public void windowOpened(WindowEvent e){}
}