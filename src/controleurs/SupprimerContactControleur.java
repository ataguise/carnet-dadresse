package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

import modele.*;
import vue.*;

public class SupprimerContactControleur extends Controleur{
	public SupprimerContactControleur(Fenetre vue, Carnet modele){
		super(vue, modele);
	}

	public void actionPerformed(ActionEvent event){
		Message m = new Message();
		boolean result = m.afficherMessage("Suppression", "Voulez vous vraimment supprimer ce contact ?");

		if(result){
			int id = this.vue.getFicheContact().getID();

			int idhasard = this.modele.donneContactSuivant(id);

			super.vue.changeContact(idhasard);
			
			super.modele.supprimerContact(id);

			super.vue.refreshStatut();
			super.vue.revalidate();
			super.vue.rafraichirOnglets();
		}
	}
}