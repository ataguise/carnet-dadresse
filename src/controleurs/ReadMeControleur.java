package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

import modele.*;
import vue.*;

public class ReadMeControleur extends Controleur{
	public ReadMeControleur(Fenetre vue, Carnet modele){
		super(vue, modele);
	}

	public void actionPerformed(ActionEvent event){
		Message m = new Message();
		
		m.afficherMessageInfo("ReadMe", "Validation des champs :\n- Tous les libélés des groupes doivent être du texte allant de 1 à 20 caractères puis optionnelement deux chiffre après\n- Les champs de numéro doivent être des numéros valide à savoir 10 chiffres dont les deux premier ne sont pas '08'\n- Les champs pour l'adresse doivent être de la forme : <num_rue> (rue|avenue|boulevard) <nom_rue> <code_postal> <nom_ville>\n- Les champs emails doivent être une adresse e-mail valide.\n- Pour supprimer une entrée, laissez le premier champ vide et validez ou annulez la modification");
	}
}