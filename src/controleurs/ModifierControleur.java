package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import modele.*;
import vue.*;

public class ModifierControleur extends Controleur{
	private boolean modif;

	public ModifierControleur(Fenetre vue, Carnet modele, boolean modif){
		super(vue, modele);
		this.modif = modif;
	}

	public void actionPerformed(ActionEvent event){
		super.vue.getFicheContact().activerModification(this.modif);
	}
}
