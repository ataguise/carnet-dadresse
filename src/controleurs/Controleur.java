package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import modele.*;
import vue.*;

public abstract class Controleur implements ActionListener{
	protected Fenetre vue;
	protected Carnet modele;

	public Controleur(Fenetre vue, Carnet modele){
		this.vue = vue;
		this.modele = modele;
	}

	public abstract void actionPerformed(ActionEvent event);
}