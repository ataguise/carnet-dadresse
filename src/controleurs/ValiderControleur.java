package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import modele.*;
import vue.*;

public class ValiderControleur extends Controleur{
	public ValiderControleur(Fenetre vue, Carnet modele){
		super(vue, modele);
	}

	public void actionPerformed(ActionEvent event){
		try{
			Message m = new Message();

			if(super.vue.getFicheContact().matchPatternFields()){
				this.modele.modifierContact(this.vue.getFicheContact().getID(), this.vue.getFicheContact().getPersonne());
				this.vue.getFicheContact().updatePersonne();
				this.vue.getFicheContact().rechargerContact();
				this.vue.getFicheContact().activerModification(false);
				this.vue.repaint();
				this.vue.rafraichirOnglets();
			} else {
				m.afficherAvertissement("Attention", "Les champs saisis ne sont pas écrits correctements !");
			}
		} catch (DoublonException e){
			System.out.println(e.getMessage());
		}
	}
}