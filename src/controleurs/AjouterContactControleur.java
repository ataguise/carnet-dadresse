package controleurs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

import modele.*;
import vue.*;

public class AjouterContactControleur extends Controleur{
	public AjouterContactControleur(Fenetre vue, Carnet modele){
		super(vue, modele);
	}

	public void actionPerformed(ActionEvent event){
		Message m = new Message();
		String type = m.demanderProfessionel("Nouveau contact", "Choisissez le type de contact à ajouter :");

		Personne p = new Personne("Nouveau", "Contact", 'M', (type.equals("Personnel") ? false : true));

		try{
			this.modele.ajouterContact(p);
		} catch(DoublonException e){
			System.out.println(e.getMessage());
		}

		super.vue.changeContact(p.getID());
		super.vue.refreshStatut();
		super.vue.revalidate();
		super.vue.rafraichirOnglets();
	}
}