Carnet d'adresse version 1.0
============================

Carnet d'adresses, c'est quoi :
*******************************
	
    - Le carnet d'adresses est une application qui permet de sauvegarder les 
      informations d'une personne (Numéro, Adresse, Email, ...). Elle possčde 
      plusieurs services : supprimer/ajouter une personne, modifier les
      informations d'une personne.

Comment compiler : (sous linux et windows)
******************

    - Aller dans le terminal
    - Aller dans le fichier carnetAdresses, racine du projet
    - Saisissez cette ligne de commande : javac @options @classes


Comment exécuter : (sous linux et windows)
******************

    - Saisissez cette fois cette ligne de commande : java -cp bin Application


Comment executer et compiler : (sous windows)
******************************

    - Lancez le script prompt.bat


Réalisé par :
*************

   - Ce projet ŕ été conçu par : DEFOIS Paul, BOUSCHARAIN Jonas et DAILLAND Arthur